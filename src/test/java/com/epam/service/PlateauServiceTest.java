package com.epam.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.epam.service.impl.PlateauServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class PlateauServiceTest {
    private int[] givenArray;

    private int[] expectedResult;

    private PlateauService plateauService;

    public PlateauServiceTest(int[] givenArray, int[] expectedResult) {
        this.givenArray = givenArray;
        this.expectedResult = expectedResult;
    }

    @Before
    public void init() {
        plateauService = new PlateauServiceImpl();
    }

    @Parameterized.Parameters
    public static int[][][] data() {
        return new int[][][]{
                {{1, 2, 2, 1, 4, 7, 7, 7, 8}, {1, 2}},
                {{1, 2, 2, 1, 4, 7, 7, 7, 1}, {5, 3}},
                {{1, 2, 2, 1, 4, 7, 7, 7, 7, 1}, {5, 4}},
                {{-1, 1, 1, 1, 0}, {1, 3}}
        };
    }

    @Test
    @DisplayName("Find the longest plateau")
    public void test_findTheLongestPlateau() {
        assertThat(plateauService.findLongestContiguousSequence(givenArray), is(expectedResult));
    }
}
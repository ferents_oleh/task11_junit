package com.epam.service;

import com.epam.service.impl.MinesweeperServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;

public class MinesweeperServiceTest {
    private MinesweeperService minesweeperService;

    @Test
    public void test_fieldRowLength() {
        minesweeperService = new MinesweeperServiceImpl(10, 12, 5);
        minesweeperService.generateMines();
        String[][] field = minesweeperService.getField();
        int expectedRowLength = 10;
        assertEquals(expectedRowLength, field.length);
    }

    @Test
    public void test_fieldColumnLength() {
        minesweeperService = new MinesweeperServiceImpl(10, 12, 5);
        minesweeperService.generateMines();
        String[][] field = minesweeperService.getField();
        int expectedColumnLength = 12;
        assertEquals(expectedColumnLength, field[0].length);
    }

}

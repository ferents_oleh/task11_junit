package com.epam.service;

import com.epam.service.impl.FileServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class FileCreationTest {
    private FileService fileService;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();;

    @Before
    public void init() {
        fileService = new FileServiceImpl();

    }

    @Test
    public void test_writeContentToFile() throws IOException {
        File output = temporaryFolder.newFile("test.txt");

        fileService.writeToFile(output.getPath(), "test");
        String content = String.valueOf(fileService.readFromFile(output.getPath()));
        content = content.replaceAll("\\[", "").replaceAll("]", "");
        assertEquals("test", content);
    }
}

package com.epam;

import com.epam.service.MinesweeperServiceTest;
import com.epam.service.PlateauServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PlateauServiceTest.class,
        MinesweeperServiceTest.class
})
public class AllTests {
}

package com.epam.controller.impl;

import com.epam.controller.PlateauController;
import com.epam.service.PlateauService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PlateauControllerImpl implements PlateauController {
    private PlateauService plateauService;

    public PlateauControllerImpl(PlateauService plateauService) {
        this.plateauService = plateauService;
    }

    @Override
    public void printPlateauSequence() {
        List<Integer> input = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        int number;

        do {
             number = scanner.nextInt();
            input.add(number);
        } while (number != -1);

        int[] sequence = input
                .stream()
                .mapToInt(Integer::intValue)
                .toArray();

        int[] result = plateauService.findLongestContiguousSequence(sequence);

        System.out.print("Start index = " + result[0] + " and length = " + result[1]);
    }
}

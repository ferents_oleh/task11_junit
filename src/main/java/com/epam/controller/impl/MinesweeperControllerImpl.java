package com.epam.controller.impl;

import com.epam.controller.MinesweeperController;
import com.epam.service.MinesweeperService;
import com.epam.service.impl.MinesweeperServiceImpl;

import java.util.Scanner;

public class MinesweeperControllerImpl implements MinesweeperController {
    private MinesweeperService minesweeperService;

    public MinesweeperControllerImpl() {}

    @Override
    public void printMinesweeper() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n:");
        int n = scanner.nextInt();
        System.out.println("Enter m:");
        int m = scanner.nextInt();
        System.out.println("Enter probability:");
        int p = scanner.nextInt();
        minesweeperService = new MinesweeperServiceImpl(n, m, p);
        minesweeperService.generateMines();
        String[][] field = minesweeperService.getField();
        for (String[] row : field) {
            for (String column : row) {
                System.out.print(column + " ");
            }
            System.out.println();
        }
        System.out.println("-".repeat(10));
        String[][] fieldWithMinesCount = minesweeperService.getFieldWithMinesCount();
        for (String[] row : fieldWithMinesCount) {
            for (String column : row) {
                System.out.print(column + " ");
            }
            System.out.println();
        }
    }
}

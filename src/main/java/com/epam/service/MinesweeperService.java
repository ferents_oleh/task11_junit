package com.epam.service;

public interface MinesweeperService {
    void generateMines();

    String[][] getFieldWithMinesCount();

    String[][] getField();
}

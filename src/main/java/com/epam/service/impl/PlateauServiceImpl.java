package com.epam.service.impl;

import com.epam.service.PlateauService;

public class PlateauServiceImpl implements PlateauService {
    @Override
    public int[] findLongestContiguousSequence(int[] values) {
        int biggestStartIndex = -1;
        int biggestLength = 0;
        int currentIndex = 1;
        int currentPlateauStartIndex = 1;
        int currentLength = 1;
        boolean plateauStarted = false;
        while (currentIndex < values.length) {
            if (isStartOfPlateau(currentIndex, values)) {
                plateauStarted = true;
                currentPlateauStartIndex = currentIndex;
            } else if (isEndOfPlateau(currentIndex, values)) {
                if (plateauStarted && currentLength > biggestLength) {
                    biggestLength = currentLength;
                    biggestStartIndex = currentPlateauStartIndex;
                }
                plateauStarted = false;
                currentLength = 1;
            } else {
                currentLength++;
            }
            currentIndex++;
        }
        if (biggestStartIndex < 0) {
            return new int[]{0, 0};
        }
        return new int[]{biggestStartIndex, biggestLength};
    }

    private static boolean isStartOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] < values[index];
    }

    private static boolean isEndOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] > values[index];
    }

}

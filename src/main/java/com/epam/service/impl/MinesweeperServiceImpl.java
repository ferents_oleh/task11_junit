package com.epam.service.impl;

import com.epam.service.MinesweeperService;

import java.util.Random;

public class MinesweeperServiceImpl implements MinesweeperService {
    private int n;

    private int m;

    private int p;

    private boolean[][] field;

    private String[][] displayField;

    public MinesweeperServiceImpl() {};

    public MinesweeperServiceImpl(int n, int m, int p) {
        this.n = n;
        this.m = m;
        this.p = p;
        field = new boolean[n][m];
        displayField = new String[n][m];
    }

    @Override
    public void generateMines() {
        fillWithProbability();
        for (int i = 0; i < field.length; ++i) {
            for (int j = 0; j < field[i].length; ++j) {
                if (field[i][j]) {
                    displayField[i][j] = "*";
                } else {
                    displayField[i][j] = ".";
                }
            }
        }
    }

    @Override
    public String[][] getFieldWithMinesCount() {
        for (int i = 0; i < field.length; ++i) {
            for (int j = 0; j < field[i].length; ++j) {
                if (field[i][j]) {
                    displayField[i][j] = "*";
                } else {
                    displayField[i][j] = countMines(i, j, field);
                }
            }
        }
        return displayField;
    }

    @Override
    public String[][] getField() {
        return displayField;
    }

    private void fillWithProbability() {
        for (int i = 0; i < field.length; ++i) {
            for (int j = 0; j < field[i].length; ++j) {
                field[i][j] = new Random().nextInt(p) == 1;
            }
        }
    }

    private String countMines(int row, int column, boolean[][] field) {
        int count = 0;
        for (int i = row - 1; i <= row + 1; ++i) {
            for (int j = column - 1; j <= column + 1; ++j) {
                if (i >= 0 && i < m && j >= 0 && j < n) {
                    if (field[i][j]) {
                        count++;
                    }
                }
            }
        }
        return String.valueOf(count);
    }
}

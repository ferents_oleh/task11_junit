package com.epam.service;

import java.io.IOException;
import java.util.List;

public interface FileService {
    void writeToFile(String path, String content) throws IOException;

    List<String> readFromFile(String path) throws IOException;
}

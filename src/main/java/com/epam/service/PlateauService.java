package com.epam.service;

public interface PlateauService {
    int[] findLongestContiguousSequence(int[] nums);
}

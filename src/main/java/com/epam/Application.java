package com.epam;

import com.epam.controller.impl.MinesweeperControllerImpl;
import com.epam.controller.impl.PlateauControllerImpl;
import com.epam.service.impl.PlateauServiceImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(new PlateauControllerImpl(new PlateauServiceImpl()), new MinesweeperControllerImpl());
    }
}

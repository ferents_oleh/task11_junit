package com.epam.view;

import com.epam.controller.MinesweeperController;
import com.epam.controller.PlateauController;

import java.util.*;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private PlateauController plateauController;

    private MinesweeperController minesweeperController;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu(PlateauController plateauController, MinesweeperController minesweeperController) {
        this.plateauController = plateauController;
        this.minesweeperController = minesweeperController;

        Menu menu = new Menu("Main menu", "");
        menu.putAction("Print the longest plateau", plateauController::printPlateauSequence);
        menu.putAction("Print the minesweeper", minesweeperController::printMinesweeper);

        activateMenu(menu);
    }

    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
